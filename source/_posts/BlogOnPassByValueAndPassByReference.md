<h1> Pass By Value and Pass By Reference in JavaScript </h1>

When you started learning javascript, you will come across these terms " Pass by value " and " Pass by reference ". These terms are used whenever we are
using functions. Let's understand clearly what is what.

<h2> Pass by value </h2>

Pass by value means that passing a copy of original value to the function as an argument. If any changes made to the variable inside the function, that will
not affect to the original value (i.e, which is in outside function). The original value and copied value are independent to each other and they have different space
in memory.

- All the primitive types are passed by values in javascript. Ex: undefined, null, boolean, string, numbers.
- Pass by value requires more space because of storing copies of orignal values.


```
function AddSquaresOfNumbers(number1, number2){
  number1 = number1 * number1;
  number2 = number2 * number2;
  return number1 + number2;
}

let number1 = 10;
let number2 = 30;

console.log(number1);       // It returns 10 
console.log(number2);       //  It returns 20

console.log(AddSquares(number1, number2));    // It returns 1000

console.log(number1);        // It returns 10
console.log(number2);        // It returns 20

```

In the above code, we declared two numbers i.e., number1 and number2 . Those numbers are passed as an arguments to AddSquaresOfNumbers function and 
modified those two values inside the function. After calling the function, again we are printing the number1 and number2, these values are not changed even 
modified inside the function. This is called pass by value.


<h2> Pass by reference </h2>

Pass by reference means that passing a reference to the original value to the function as an argument. If any changes made to the value inside the function
will affect the original value (i.e, which is in outside function) because reference means address of the variable. It does not create any copy for the variable.

- All the Non - primitive types are passed by reference in javascript. Ex: Object, arrays, Functions


```
let personDetails = {
                        name: "Praveen",
                        age: 22,
                        place: "Hindupur"
                    };
                    

function changeAge(personDetails) { 
  personalDetails.age = 23; 
  console.log(personalDetails.age); 
} 

console.log(personalDetails.age);         // 22
changeAge(personalDetails);               // 23
console.log(personalDetails.age);         // 23


```

In the above code, an object is declared and passed as an argument to a function and change the property value of an object inside the function
and those propery value of an object will affect to the original values also which is in the outside loop. As you can see the output of the age property value
changed after calling the function. This is pass by reference.


<h2> Conclusion </h2>

Pass by value and pass by reference are two ways to pass values to function and it is important to understand these concepts when working with complex data.
I hope this above information is used to get an idea of pass by value and pass by reference.
